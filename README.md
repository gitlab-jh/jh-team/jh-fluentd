Ansible role: jh-fluentd
=========

Ansible role to deploy fluentd agents in Jihu Gitlab SaaS VMs.

Example Playbook
----------------

Including an example of how to use your role (for instance, with variables passed in as parameters) is always nice for users too:
```yaml
- hosts: all:!ungrouped
  become: yes
  roles:
    - jh-fluentd
  vars:
    fluentd_version: 4
    fluentd_package_state: present
    fluentd_service_name: td-agent
    fluentd_service_state: started
    fluentd_service_enabled: true
    kafka:
      enabled: true
      chunk_limit_size: 500000
      brokers:
      - < kakfa borker >
```

Install Fluentd plugins
----------------
Currently need to specify plugins for certain groups.  

**group_vars/redis.yml**

```yaml
fluentd_plugins:
- name: fluent-plugin-redis-slowlog
- name: fluent-plugin-kafka
```

**group_vars/postgres.yml**

```yaml
fluentd_plugins:
- name: fluent-plugin-postgresql-csvlog
- name: fluent-plugin-kafka
```